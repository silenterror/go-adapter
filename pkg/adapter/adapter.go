package adapter

import (
	"fmt"
)

// storage - contains a store to store our records
type storage struct {
	store []interface{}
}

// NewStorage - returns a new storage
func NewStorage() *storage {
	return &storage{
		store: []interface{}{},
	}
}

// recordInserter - inserts records into the storage via the method allows the adapter to have this method
// to store things other than a record in needed
type recordInserter interface {
	InsertRecord(s *storage)
}

// InsertRecord - inserts a record into the storage
func (s *storage) InsertRecord(r recordInserter) {
	fmt.Println("using storage method insertRecord()")
	r.InsertRecord(s)
}

// GetStorage - prints the store
func (s *storage) GetStorage() []interface{} {
	for i := 0; i < len(s.store); i++ {
		fmt.Println(fmt.Sprintf("%v", s.store[i]))
	}
	return s.store
}

// Record - the record type
type Record struct {
	Name string
	Id   int
}

// InsertRecord - method for record that takes the storage and inserts it into the store
func (r *Record) InsertRecord(s *storage) {
	s.store = append(s.store, r)
}

// Item - the item type
type Item struct {
	Name  string
	Valid bool
}

// InsertItem - method for item that takes storage and inserts it into the store
func (i *Item) InsertItem(s *storage) {
	s.store = append(s.store, i)
}

// NewRecord - creates a new record
func NewRecord(n string, id int) *Record {
	return &Record{
		Name: n,
		Id:   id,
	}
}

// NewItem - creates a new item
func NewItem(n string, b bool) *Item {
	return &Item{
		Name:  n,
		Valid: b,
	}
}

// **************************************************
// Adapter Section with type, constructor and methods
// **************************************************

// ItemAdapter - contains the item to insert.
type ItemAdapter struct {
	Item *Item
}

// NewAdapter - returns a new adapter with the supplied item
func NewAdapter(i *Item) *ItemAdapter {
	return &ItemAdapter{
		Item: i,
	}
}

// InsertRecord - method of the itemAdapter that makes the adapter also a type inserter.
// by also being an inserter we can insert an item that is not a record
func (i *ItemAdapter) InsertRecord(s *storage) {
	fmt.Println("using the adapter to insert an item into storage")
	i.Item.InsertItem(s)
}
