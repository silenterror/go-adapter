package adapter

import (
	"fmt"
	"reflect"
	"testing"
)

var (

)

// TestNewStorage - tests the storage return
func TestNewStorage(t *testing.T) {
	got := NewStorage()
	gotType := reflect.ValueOf(*got).Kind()
	wantType := reflect.ValueOf(storage{}).Kind()

	if gotType != wantType {
		t.Errorf("got %v want %v\n", gotType, wantType)
	}
}

// TestNewRecord - tests the new record return
func TestNewRecord(t *testing.T) {
	test := Record{
		Name: "foo",
		Id:   1,
	}
	got := NewRecord("foo", 1)
	gotType := reflect.ValueOf(*got).Kind()
	wantType := reflect.ValueOf(test).Kind()

	if gotType != wantType {
		t.Errorf("got %v want %v\n", gotType, wantType)
	}

	if test.Name != got.Name || test.Id != got.Id {
		t.Errorf("fields do not match: got: %v want: %v\n", got, test)
	}
}

// TestNewItem - tests the new item return
func TestNewItem(t *testing.T) {
	test := Item{
		Name: "foo",
		Valid:   true,
	}
	got := NewItem("foo", true)
	gotType := reflect.ValueOf(*got).Kind()
	wantType := reflect.ValueOf(test).Kind()

	if gotType != wantType {
		t.Errorf("got %v want %v\n", gotType, wantType)
	}

	if test.Name != got.Name || test.Valid != got.Valid {
		t.Errorf("fields do not match: got: %v want: %v\n", got, test)
	}
}

// TestStorage_InsertRecord - tests the InsertRecord method of storage
func TestStorage_InsertRecord(t *testing.T) {
	testStorage := NewStorage()
	rec := NewRecord("foo", 1)
	testStorage.InsertRecord(rec)

	recPull, ok := testStorage.store[0].(*Record)
	if ok && recPull.Name == "foo" && recPull.Id == 1 {
		fmt.Println(recPull)
	}
	if !ok {
		fmt.Println(recPull)
		t.Error("could not pull record")
	}
}

//TestGetStorage_GetStorage - tests the get storage method of storage
func TestStorage_GetStorage(t *testing.T) {
	testStorage := NewStorage()
	rec := NewRecord("foo", 1)
	testStorage.InsertRecord(rec)
	recPull := testStorage.GetStorage()
	recCast, ok := recPull[0].(*Record)
	if !ok || recCast.Name != "foo" || recCast.Id != 1 {
		t.Error("could not pull record")
	}

}

func TestItemAdapter(t *testing.T) {
	want := Item{
		Name:  "foo",
		Valid: true,
	}
	itm := NewItem("foo", true)
	adptr := NewAdapter(itm)

	testStorage := NewStorage()
	testStorage.InsertRecord(adptr)

	got , ok := testStorage.store[0].(*Item)

	if !ok || want.Name != got.Name || want.Valid != got.Valid {
		t.Errorf("fields do not match: got: %v want: %v\n", got, want)
	}
}
