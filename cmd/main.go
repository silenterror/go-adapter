package main

import (
	"fmt"
	"gitlab.com/silenterror/adapter/pkg/adapter"
)

/*
	The demonstration is about having the storage take an interface that contains a method to insert into the store.
	The storage has a method called insertRecord(r recordInserter) which takes an inteface called recordInserter.
	As long as the type implents that interface it can be used to insert something into the store.  By making an adapter
	that implements that insertRecord you can then put the item in that adapter and add it to the storage.

*/

func main() {
	fmt.Println("starting adapter example")

	// create the storage
	s := adapter.NewStorage()

	// print the current storage
	s.GetStorage()

	// create the record and the item
	itm := adapter.NewItem("itemA", true)
	rec := adapter.NewRecord("recordA", 1)

	// insert the expected record
	s.InsertRecord(rec)

	// create the adapter with the item to insert into storage
	itmAdapter := adapter.NewAdapter(itm)

	// now the insert record will take the adapter and insert the item
	// even though it is not a record or itemInserter because it takes the interface type
	// which the adapter implements as it has a method called InsertRecord
	s.InsertRecord(itmAdapter)

	// print the storage showing both the record and item in the store
	i := s.GetStorage()
	for _,v := range i {
		fmt.Println(v)
	}
}